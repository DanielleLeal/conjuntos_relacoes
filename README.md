## Trabalho prático sobre Conjuntos e Relações
Integrantes: Danielle Leal Silva e Lucas de Oliveira Souza Barbosa
Instituição: Instituto Federal de Educação Ciência e Tecnologia de Minas Gerais - Campus Formiga
2° período de Ciência da Computação

## Linguagem
C é uma linguagem de programação compilada de propósito geral, estruturada, imperativa, procedural, padronizada pela Organização Internacional para Padronização, criada em 1972 por Dennis Ritchie na empresa AT&T Bell Labs para desenvolvimento do sistema operacional Unix

## IDE
O NetBeans IDE é um ambiente de desenvolvimento integrado gratuito e de código aberto para desenvolvedores de software nas linguagens Java, JavaScript, HTML5, PHP, C/C++, Groovy, Ruby, entre outras. O IDE é executado em muitas plataformas, como Windows, Linux, Solaris e MacOS
